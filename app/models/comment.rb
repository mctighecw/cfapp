class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :product

	validates :body, length: {
    minimum: 3,
    maximum: 50
  }

  validates :user, presence: true
  validates :product, presence: true
  validates :rating, numericality: { only_integer: true }

  # Add comment to product show page
  after_create_commit { CommentUpdateJob.perform_now(self, @user) }
end