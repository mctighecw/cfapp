class ApplicationMailer < ActionMailer::Base
  default from: "mctighecw@gmail.com"
  layout 'mailer'
end