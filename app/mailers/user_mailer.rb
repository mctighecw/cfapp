class UserMailer < ApplicationMailer
  default from: "mctighecw@gmail.com"

  # Welcome message to new user on sign up
  def welcome(name, email)
    @name = name
    @email = email
    mail( :to => email,
          :subject => "Welcome to Oasis Books")
  end

  # Contact form submission information email to admin
  def contact_info(name, company, email, telephone, message)
    @name = name
    @company = company
    @email = email
    @telephone = telephone
    @message = message
    mail( :to => "mctighecw@gmail.com",
          :subject => "Oasis Books Contact Form")
  end

  # Contact form submission email reply to sender
  def contact_reply(email, name, message)
    @email = email
    @name = name
    @message = message
    mail( :to => email,
          :subject => "Thank You for Your Message")
  end

  # Order and payment confirmation email to customer
  def order_placed(name, title, id, price, email)
    @name = name
    @title = title
    @id = id
    @price = price
    @email = email
    mail( :to => email,
          :subject => "Your Order from Oasis Books")
  end

end