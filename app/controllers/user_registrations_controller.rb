class UserRegistrationsController < Devise::RegistrationsController
  def create
    super
    if @user.persisted?
      # Send welcome email to new user after sign up
      @name = current_user.first_name
      @email = current_user.email
      UserMailer.welcome(@name, @email).deliver_now
    end
  end

end