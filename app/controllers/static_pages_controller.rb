class StaticPagesController < ApplicationController
  def index
    # Featured products carousel
    @products = Product.limit(3)
  end

  # Product search page
  def search
  end

  # Product search results page
  def search_results
  end

  def about
  end

  def contact
  end

  # Contact form submission email sent to admin and sender
  def thank_you
    @name = params[:name]
    @company = params[:company]
    @email = params[:email]
    @telephone = params[:telephone]
    @message = params[:message]
    UserMailer.contact_reply(@email, @name, @message).deliver_now
    UserMailer.contact_info(@name, @company, @email, @telephone, @message).deliver_later(wait: 0.25.hour)
  end

end