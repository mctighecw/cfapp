class PaymentsController < ApplicationController
  def create
    @product = Product.find(params[:product_id])
    @user = current_user
    token = params[:stripeToken]
    # Create the charge on Stripe's servers - this will charge the user's card
    begin
      charge = Stripe::Charge.create(
        :amount => (@product.price*100).to_i, # amount in cents
        :currency => "usd",
        :source => token,
        :description => params[:stripeEmail]
      )

      if charge.paid
        # If the card is accepted
        # Create new order with product ID, User ID, product price
        Order.create!(product_id: @product.id, user_id: @user.id, total: @product.price)

        # Send e-mail confirmation to customer
        @name = current_user.first_name
        @title = @product.title
        @id = @product.id
        @price = @product.price
        @email = current_user.email
        UserMailer.order_placed(@name, @title, @id, @price, @email).deliver_later(wait: 0.25.hour)
      end

      flash[:success] = "Payment has been processed successfully"

    rescue Stripe::CardError => e
      # If the card is declined
      body = e.json_body
      err = body[:error]
      flash[:error] = "Unfortunately, there was an error processing your payment: #{err[:message]}"
    end

    # Load payment thank you page
    redirect_to payments_payment_thank_you_path
  end

  # Payment thank you page
  def payment_thank_you
  end

end