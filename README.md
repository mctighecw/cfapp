# README

This is my first Career Foundry Rails App. It is a basic e-commerce site.

## App Information

App Name: cfapp (Oasis Books)

Created: May 2016; updated 2018, 2020, 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/cfapp)

## To Run

1. Fill out `.env` file values

2. Build Docker image and start container

```sh
$ docker-compose up -d --build
```

3. Set up database (if not already done)

```sh
$ docker exec -it oasis-books bash

> rake db:create
> rake db:migrate
> rake db:seed
> exit
```

Project is running at `http://localhost:3000`

## Notes

- Ruby version 2.3.1

- Rails version 5.0.0 (project started with 4.2.6)

- Databases:

  - development: SQLite3
  - production: PostgreSQL

- Many gems installed: Rubocop, Turbolinks, Twitter-Bootstrap-Rails, Devise, CanCanCan, etc.

- Stripe service used for payments (only test mode is configured)

- Memcachier and Redis are configured for caching

- ActionCable (Rails 5) is set up and working

- AngularJS is used for some pages (Orders)

- No test suite installed

- Originally deployed on [Heroku](https://www.heroku.com)

Please feel free to send me comments and give some feedback.

Last updated: 2024-12-28
