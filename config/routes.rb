Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, :controllers => { :registrations => "user_registrations" }
  resources :users
  resources :orders, only: [:index, :show, :create, :update, :destroy]

  resources :products do
    resources :comments
  end

  mount ActionCable.server => '/cable'

  # You can have the root of your site routed with "root"
  root 'static_pages#index'

  get '/search', to: 'static_pages#search'

  get '/search_results', to: 'products#search_results'

  get '/about', to: 'static_pages#about'

  get '/contact', to: 'static_pages#contact'

  post '/thank_you', to: 'static_pages#thank_you'

  devise_scope :user do
    get '/sign_up', to: 'devise/registrations#new'
    get '/profile', to: 'devise/registrations#edit'
  end

  # Stripe credit card payments - payments controller
  post '/payments/create', to: 'payments#create'
  get 'payments/payment_thank_you', to: 'payments#payment_thank_you'

end
